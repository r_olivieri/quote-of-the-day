# Anomaly: Author and Rating Services Slow

This use case executes the following steps:

1. Two new types of log entries are introduced in the Author and Rating service.  The first is of the form:

```
Incoming request for resource {{URL}} source: {{IP}} 
```
It repeats nominally eveyr 3 seconds.

The second of the form:

```
WARNING incoming {{VERB}} request from {{IP}} rejected 
```

It repeats about every second.

The use case pauses for 2 minutes.

2. After the 2 minutes, the Author and Rating services experience additional 0.8 second delays.  

3. The use case pauses for a minute.

4. The Author and Rating service delay extends to 2 seconds nominally.

5. Another minute pause.

6. Both the Author and Rating service crash.

## Log Anomaly

The log anomaly appears in the log aggregator as:

![elk](images/ar_kibana.png)

## Instana View




## Source

```json
{
    "id": "author_rating_slow",
    "name": "Author and Ratings Service Slow",
    "description": "This use case progressively makes both the author and rating service slower.  This is preceded with new log entry types in both services.  Eventually both services crash.",
    "type": "anomaly",
    "steps": [
        {
            "name": "Start log warning about requests for resource fro IP address (rating service).  Repeats every 3 seconds.",
            "type": "add_logger",
            "service": "rating",
            "log": {
                "id": "resreq1",
                "template": "Incoming request for resource {{URL}} source: {{IP}} ",
                "fields": {
                    "URL": {
                        "type": "url"
                    },
                    "IP": {
                        "type": "ip"
                    }
                },
                "repeat": {
                    "mean": 3000,
                    "stdev": 100,
                    "min": 2000,
                    "max": 4000
                }
            }
        },
        {
            "name": "Start log warning about requests for resource fro IP address (author service).  Repeats every 3 seconds.",
            "type": "add_logger",
            "service": "rating",
            "log": {
                "id": "resreq1",
                "template": "Incoming request for resource {{URL}} source: {{IP}} ",
                "fields": {
                    "URL": {
                        "type": "url"
                    },
                    "IP": {
                        "type": "ip"
                    }
                },
                "repeat": {
                    "mean": 3000,
                    "stdev": 100,
                    "min": 2000,
                    "max": 4000
                }
            }
        },
        {
            "name": "Start log warning about incoming HTTP requests from external IP address (rating service).  Repeats about every second.",
            "type": "add_logger",
            "service": "rating",
            "log": {
                "id": "in_req",
                "template": "WARNING incoming {{VERB}} request from {{IP}} rejected ",
                "fields": {
                    "IP": {
                        "type": "ip"
                    },
                    "VERB": {
                        "type": "pickWeighted",
                        "options": [
                            { "name": "GET", "value": "GET", "weight": 8 },
                            { "name": "POST", "value": "POST", "weight": 3 },
                            { "name": "PUT", "value": "PUT", "weight": 1 },
                            { "name": "DELETE", "value": "DELETE", "weight": 1 },
                            { "name": "HEAD", "value": "HEAD", "weight": 7 }
                        ]
                    }
                },
                "repeat": {
                    "mean": 1000,
                    "stdev": 500,
                    "min": 10,
                    "max": 3000
                }
            }
        },
        {
            "name": "Start log warning about incoming HTTP requests from external IP address (author service).  Repeats about every second.",
            "type": "add_logger",
            "service": "author",
            "log": {
                "id": "in_req",
                "template": "WARNING incoming {{VERB}} request from {{IP}} rejected ",
                "fields": {
                    "IP": {
                        "type": "ip"
                    },
                    "VERB": {
                        "type": "pickWeighted",
                        "options": [
                            { "name": "GET", "value": "GET", "weight": 8 },
                            { "name": "POST", "value": "POST", "weight": 3 },
                            { "name": "PUT", "value": "PUT", "weight": 1 },
                            { "name": "DELETE", "value": "DELETE", "weight": 1 },
                            { "name": "HEAD", "value": "HEAD", "weight": 7 }
                        ]
                    }
                },
                "repeat": {
                    "mean": 1000,
                    "stdev": 500,
                    "min": 10,
                    "max": 3000
                }
            }
        },
        {
            "name": "Pause for 2 min",
            "type": "delay",
            "duration": 120000
        },
        {
            "name": "Increase rating service delay (0.8s)",
            "type": "metric",
            "service": "rating",
            "metric": "apiRes",
            "value": {
                "mean": 800,
                "stdev": 200,
                "min": 200,
                "max": 1100
            }
        },
        {
            "name": "Increase author service delay (0.8s)",
            "type": "metric",
            "service": "author",
            "metric": "apiRes",
            "value": {
                "mean": 800,
                "stdev": 200,
                "min": 200,
                "max": 1100
            }
        },         
        {
            "name": "Pause for 1 min",
            "type": "delay",
            "duration": 60000
        },
        {
            "name": "Increase rating service delay (2s)",
            "type": "metric",
            "service": "rating",
            "metric": "apiRes",
            "value": {
                "mean": 2000,
                "stdev": 200,
                "min": 200,
                "max": 1100
            }
        },
        {
            "name": "Increase author service delay (2s)",
            "type": "metric",
            "service": "author",
            "metric": "apiRes",
            "value": {
                "mean": 2000,
                "stdev": 200,
                "min": 200,
                "max": 1100
            },
            "ramp": 100000
        },         
        {
            "name": "Pause for 1 min",
            "type": "delay",
            "duration": 60000
        },
        {
            "name": "Crash rating service",
            "type": "service_action",
            "service": "rating",
            "action": "crash"
        },
        {
            "name": "Pause for a min",
            "type": "delay",
            "duration": 60000
        },
        {
            "name": "Crash author service",
            "type": "service_action",
            "service": "author",
            "action": "crash"
        }
    ]
}
```